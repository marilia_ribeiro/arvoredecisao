#coding:utf-8
base = {
	'atributos': 
	[
		'motor', 
		'rodas',
		'porte'
	],
	'data': 
	[
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "pequeno",
			"rotulo": "carro"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "pequeno",
			"rotulo": "carro"
		},
		{
			"motor": "nao",
			"rodas": 2,
			"porte": "pequeno",
			"rotulo": "bicicleta"
		},
		{
			"motor": "sim",
			"rodas": 2,
			"porte": "pequeno",
			"rotulo": "moto"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "pequeno",
			"rotulo": "carro"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "pequeno",
			"rotulo": "carro"
		},
		{
			"motor": "sim",
			"rodas": 2,
			"porte": "pequeno",
			"rotulo": "moto"
		},
		{
			"motor": "nao",
			"rodas": 2,
			"porte": "pequeno",
			"rotulo": "bicicleta"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "grande",
			"rotulo": "caminhao"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "pequeno",
			"rotulo": "carro"
		},
		{
			"motor": "nao",
			"rodas": 2,
			"porte": "pequeno",
			"rotulo": "bicicleta"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "medio",
			"rotulo": "caminhote"
		},
		{
			"motor": "sim",
			"rodas": 2,
			"porte": "pequeno",
			"rotulo": "moto"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "medio",
			"rotulo": "caminhote"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "pequeno",
			"rotulo": "carro"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "medio",
			"rotulo": "caminhote"
		},
		{
			"motor": "sim",
			"rodas": 2,
			"porte": "pequeno",
			"rotulo": "moto"
		},
		{
			"motor": "nao",
			"rodas": 2,
			"porte": "pequeno",
			"rotulo": "bicicleta"
		},
		{
			"motor": "nao",
			"rodas": 2,
			"porte": "pequeno",
			"rotulo": "bicicleta"
		},
		{
			"motor": "sim",
			"rodas": 2,
			"porte": "pequeno",
			"rotulo": "moto"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "pequeno",
			"rotulo": "carro"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "medio",
			"rotulo": "caminhote"
		},
		{
			"motor": "nao",
			"rodas": 2,
			"porte": "pequeno",
			"rotulo": "bicicleta"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "grande",
			"rotulo": "caminhao"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "medio",
			"rotulo": "caminhote"
		},
	]
}
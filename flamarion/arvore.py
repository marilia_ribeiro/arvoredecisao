#coding: utf-8
from entropia import Entropia
from atributo import Atributo
from no import No
from ramo import Ramo
from folha import Folha
from EntropyAndGain import *
import copy

class Arvore:
    
    raiz = None
    class_name = "class"
    profundidade = 0
    acertos_no_teste = 0

    def induzirArvore(self, atributos, exemplos):

        if len(exemplos) == 0:
            return -1 #Erro
        if len(atributos) == 0: # Se acabaram os atributos
            folha = self.setClasseMaisFrequente(exemplos)
            return folha # retorna uma folha com a classe mais repetida
        else:
            registros_para_entropia = self.preparaDadosParaEntropia(atributos, exemplos)

            Calculating_end_gain = EntropyAndGain(registros_para_entropia, self.class_name)
            Calculating_end_gain.calculatingEntropyOfTheClass()
            atributo_com_melhor_ganho = Calculating_end_gain.calculatingEntropyOfTheAttributes()
        
            atributo = atributo_com_melhor_ganho[0]
            ganho = atributo_com_melhor_ganho[1]

            if ganho == 0:
                folha = self.setClasseMaisFrequente(exemplos)
                return folha

            atributos_nao_utilizados = list(atributos) #copia lista de atributos para uma lista de atributos ainda não utilizados
            del atributos_nao_utilizados[atributos.index(atributo)] #remove atributo recem utilizado, mantendo apenas os inexplorados
            valores = self.separarRegistros(atributo, exemplos) # Encontra a variação de valores para aquele atributo

            no = No(atributo) # instancia NÓ            
            
            if self.raiz == None: # se RAIZ ainda nao foi definida, atribui a ela o NÓ recém criado
                self.raiz = no

            for valor, exemplos_segmentados in valores.items(): # para cada possível valor do atributo
                ramo = Ramo(valor, no) # cria o ramo para aquele valor definindo a ORIGEM do ramo como o NÓ examinado nessa recursão
                no.addRamo(ramo) # liga o ramo ao nó
                ramo.destino = self.induzirArvore(atributos_nao_utilizados, exemplos_segmentados) # O nó destino será descoberto na próxima recursão
            
            return no # Após examinar todos os RAMOS (valores), retorna o NÓ totalmente explorado


    def separarRegistros(self, atributo, exemplos):
        valores = {}
        for exemplo in exemplos: # para cada exemplo do conjunto de exemplos
            if exemplo[atributo] not in valores: # se o valor ainda não foi descoberto
                valores[exemplo[atributo]] = [exemplo] # crie a chave no dicionário de valores e atribua o registro para essa chave
            else:
                valores[exemplo[atributo]].append(exemplo) # se já existe a chave, apenas adicione o registro na lista de registros que possuem aquele valor
        return valores

    def setClasseMaisFrequente(self, exemplos):
        # Quando é preciso descobrir a FOLHA da árvore de decisão
        classes = {}
        for exemplo in exemplos: # percorre todos os exemplos contidos naquele ponto do treinamento da árvore
            # Isso para descobrir todas as classes e as frequências delas
            if exemplo[self.class_name] in classes:
                classes[exemplo[self.class_name]] += 1  
            else:
                classes[exemplo[self.class_name]] = 1

        # Após descobrir todas as classes e as frequencias
        classe_mais_frequente, maior_frequencia = None, 0

        for classe, frequencia in classes.items(): # percorre as classes
            if frequencia >= maior_frequencia: # a fim de encontrar a classe com maior frequencia
                maior_frequencia = frequencia
                classe_mais_frequente = classe

        folha =  Folha(classe_mais_frequente) # Finalmente, a classe com maior frequencia é atribuída para uma nova folha da árvore
        return folha


    def preparaDadosParaEntropia(self, atributos, exemplos):
        cabecalho = [self.class_name]+atributos
        registros_para_entropia = [cabecalho]
        for exemplo in exemplos:
            linha = []
            for coluna in cabecalho:
                linha.append(exemplo[coluna])
            registros_para_entropia.append(linha)
        return registros_para_entropia
    
    def classificarItensDesconhecidos(self, raiz, conjunto_de_testes):
        print("\n================ VALIDAÇÃO ÁRVORE ================")
        
        for registro_teste in conjunto_de_testes:
            valor = self.classificarRegistro(raiz, registro_teste)
            #print(registro_teste[self.class_name])

        total_de_testes = len(conjunto_de_testes)
        acuracia = float(self.acertos_no_teste) / float(total_de_testes) * 100
        print("ACURÁCIA DO MODELO: %4.1f%%\n\t%i acerto(s)\n\t%i erro(s)\n\tEm %i testes realizados" %(acuracia, self.acertos_no_teste, (total_de_testes - self.acertos_no_teste), total_de_testes))
        

    def classificarRegistro(self, item, teste):
        # Algoritmo que percorre a árvore
        if isinstance(item, No): # Se o item atual for um (NÓ)
            no_atual = item.desc
            if no_atual in teste: # Se o item de dado possui um registro para aquele atributo do NÓ
                valor_do_teste = teste[no_atual] # pega o valor do registro para o atributo do NÓ
            else:
                self.mostrarErro(teste, "Nao possui atributo "+str(no_atual).upper()) # senão é um erro: houve uma inconsistência no pré-processamento
                return

            caminho = None #caminho iniciado nulo
            for ramo in item.ramos: # são percorridos os RAMOS do NÓ, ou seja, os possíveis valores do atributo
                if ramo.desc == str(no_atual)+"_"+str(valor_do_teste): # se o ramo/valor for igual ao valor do registro do item de teste
                    caminho = ramo # atribui aquele RAMO como o caminho para prosseguir na árvore
                    break
            
            if caminho == None: # se não foi encontrado um caminho
                self.mostrarErro(teste, "Nao existem mais caminhos para classificar o NO") # informa que não existem caminhos mais... houve alguma incosistência
            else: # se há um caminho
                self.classificarRegistro(caminho.destino, teste) # Prosseguir o percorrimento da árvore passando o destino (Um NÓ ou uma folha) do caminho e novamente o item de dados teste
        
        if isinstance(item, Folha): # Se o item atual for uma (FOLHA)
            rotulo_da_folha = item.desc # pega o rótulo que a folha remete
            if self.class_name in teste: # verifica se há a coluna que indica a classe no registro teste
                if teste[self.class_name] == rotulo_da_folha: # Compara a classe/rótulo real do dado com a classe/rótulo encontrada no percorrimento da árvore
                    print("[ACERTO] Registro: %s - Classificacao: %s" %(str(teste[self.class_name]), str(rotulo_da_folha)))
                    self.acertos_no_teste +=1 # Como é igual, aumenta a acurácia em 1
                    return True
                else:
                    print("[_ERRO_] Registro: %s - Classificacao: %s" %(str(teste[self.class_name]), str(rotulo_da_folha)))
                    return False
            else:
                teste[self.class_name] = item.desc
                return False
            self.mostrarRotulo(teste, item.desc)

    def mostrarErro(self, registro, mensagem):
        print("[_ERROR_] Registro %s : %s" %(str(registro), mensagem))

    def visualizar(self):
        self.preOrdem(self.raiz, 0)

    def preOrdem(self, item, profundidade):
        espacamento = profundidade*"|\t"
        if isinstance(item, No):
            print(espacamento+"|%s]" %item.desc)
            for ramo in item.ramos:
                print(espacamento+"|__"+str(ramo.valor).upper())
                self.preOrdem(ramo.destino, profundidade+1)
        if isinstance(item, Folha):
            print(espacamento+"|---> class: %s" %str(item.desc))

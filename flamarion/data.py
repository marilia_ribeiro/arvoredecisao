#coding:utf-8
base = {
	'atributos': 
	[
		'motor', 
		'rodas',
		'porte'
	],
	'treinamento': 
	[
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "pequeno",
			"class": "carro"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "pequeno",
			"class": "carro"
		},
		{
			"motor": "nao",
			"rodas": 2,
			"porte": "pequeno",
			"class": "bicicleta"
		},
		{
			"motor": "sim",
			"rodas": 2,
			"porte": "pequeno",
			"class": "moto"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "pequeno",
			"class": "carro"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "pequeno",
			"class": "carro"
		},
		{
			"motor": "sim",
			"rodas": 2,
			"porte": "pequeno",
			"class": "moto"
		},
		{
			"motor": "nao",
			"rodas": 2,
			"porte": "pequeno",
			"class": "bicicleta"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "grande",
			"class": "caminhao"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "pequeno",
			"class": "carro"
		},
		{
			"motor": "nao",
			"rodas": 2,
			"porte": "pequeno",
			"class": "bicicleta"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "medio",
			"class": "caminhote"
		},
		{
			"motor": "sim",
			"rodas": 2,
			"porte": "pequeno",
			"class": "moto"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "medio",
			"class": "caminhote"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "pequeno",
			"class": "carro"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "medio",
			"class": "caminhote"
		},
		{
			"motor": "sim",
			"rodas": 2,
			"porte": "pequeno",
			"class": "moto"
		},
		{
			"motor": "nao",
			"rodas": 2,
			"porte": "pequeno",
			"class": "bicicleta"
		},
		{
			"motor": "nao",
			"rodas": 2,
			"porte": "pequeno",
			"class": "bicicleta"
		},
		{
			"motor": "sim",
			"rodas": 2,
			"porte": "pequeno",
			"class": "moto"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "pequeno",
			"class": "carro"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "medio",
			"class": "caminhote"
		},
		{
			"motor": "nao",
			"rodas": 2,
			"porte": "pequeno",
			"class": "bicicleta"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "grande",
			"class": "caminhao"
		},
		{
			"motor": "sim",
			"rodas": 4,
			"porte": "medio",
			"class": "caminhote"
		},
	],
	'testes':
	[
		{
			'motor': 'sim',
			'rodas': 4,
			'porte': 'pequeno'
		},
		{
			'motor': 'nao',
			'rodas': 2,
			'porte': 'pequeno'
		},
		{
			'motor': 'sim',
			'rodas': 2,
			'porte': 'pequeno'
		},
		{
			'rodas': 4,
			'porte': 'grande'
		},
		{
			'motor': 'sim',
			'rodas': 4,
			'porte': 'medio'
		},
		{
			'motor': 'nao',
			'rodas': 2,
			'porte': 'pequeno'
		}
	]
}
#!/usr/bin/env python
#coding= utf-8
#from scipy.stats import entropy, norm
import math
from statistics import median



class EntropyAndGain:
    '''
    class_name = Nome do Atributo que deve ser considerado como Classe/tag
    entropy_class = chame função calculatingEntropyOfTheClass e obtenha o resultado do calcula da entropia da classe.
    entropy_and_gain_attributes = obtenha o resultado do ganho dos atributos contidos na base
    data = Dados que serão minerados
    dataList = Dados separados em listas, onde lista[0] = Atributos e, demais posições= Registros
    full_record = Total de Registros
    '''

    #class_name = "rotulo"
    class_name = ""
    entropy_class = None
    entropy_and_gain_attributes = {}
    #data = open("wine_training2.txt", "r")
    #data_list = [record.rstrip().split(",") for record in data]
    #data_list = [['rotulo', 'color_intensity', 'diluted_wines', 'malic_acid', 'alcohol'], ['1', 'min', 'max', 'max', 'max'], ['1', 'max', 'max', 'max', 'max'], ['1', 'max', 'max', 'max', 'max'], ['1', 'min', 'max', 'max', 'max'], ['1', 'max', 'max', 'max', 'max'], ['1', 'max', 'max', 'max', 'max'], ['2', 'min', 'min', 'min', 'min'], ['2', 'min', 'min', 'min', 'min'], ['2', 'max', 'min', 'min', 'min'], ['2', 'min', 'max', 'min', 'max'], ['2', 'min', 'max', 'min', 'min'], ['2', 'min', 'min', 'min', 'min'], ['2', 'min', 'max', 'min', 'min'], ['3', 'min', 'min', 'min', 'max'], ['3', 'max', 'min', 'max', 'max'], ['3', 'max', 'min', 'max', 'min'], ['3', 'min', 'min', 'max', 'min'], ['3', 'max', 'min', 'min', 'min'], ['3', 'max', 'min', 'max', 'min']]
    #class_position = data_list[0].index(class_name)
    data_list = []
    class_position = None
    #full_record = len(data_list)-1
    full_record = None

    def __init__(self, data_list, class_name):
        self.data_list = data_list
        self.class_name = class_name
        self.class_position = data_list[0].index(self.class_name)
        self.full_record = len(data_list)-1

    def dataDistributionByClass(self, attribute):
        '''Montar  distribuição de dados da classe'''
        data_distribution = {}

        try:
            attribute_position = self.data_list[0].index(attribute)
        except:
            return "Erro, Atributo não encontrado"

        for record in self.data_list[1:]:
            if data_distribution.get(record[attribute_position]):
                data_distribution[record[attribute_position]] += 1
            else:
                data_distribution[record[attribute_position]] = 1
        return data_distribution


    def defineProbabilityByClass(self,distribution_attribute,):
        '''Calcular a tabela de Probabilidade DA CLASSE'''
        data_probability = []
        list_keys = distribution_attribute.keys()
        for key in list_keys:
            data_probability.append({
                "key": key,
                "distribution": distribution_attribute.get(key),
                "probability": (float(distribution_attribute.get(key))/float(self.full_record))
            })

        return data_probability

    def dataDistributionByAttribute(self):
        '''percorre todos atributos verificando a quantidade de distribuição, de atributos por classe
            conforme a discretização dos dados
        '''

        distribution_attributes = []


        for attribute in self.data_list[:1][0]:
            data_distribution = {}

            #attribute  = "alcohol"

            try:
                attribute_position = self.data_list[0].index(attribute)
            except:
                return "Erro, Atributo não encontrado"

            #mediana_attribute = self.mediana(attribute_position)
            data_distribution["attribute"] = attribute
            #data_distribution["mediana"] = mediana_attribute

            for record in self.data_list[1:]:
                if data_distribution.get(record[self.class_position]):
                    #if float((record[attribute_position])) > float(mediana_attribute):
                    if record[attribute_position] == "max":
                        if data_distribution[record[self.class_position]].get("max"):
                            data_distribution[record[self.class_position]]["max"] += 1
                        else:
                            data_distribution[record[self.class_position]]["max"] = 1
                    else:
                        if data_distribution[record[self.class_position]].get("min"):
                            data_distribution[record[self.class_position]]["min"] += 1
                        else:
                            data_distribution[record[self.class_position]]["min"] = 1
                else:
                    data_distribution[record[self.class_position]] = {}
                    data_distribution[record[self.class_position]]["max"] = 0
                    data_distribution[record[self.class_position]]["min"] = 0
                    #if float(record[attribute_position]) > float(mediana_attribute):
                    if record[attribute_position] == "max":
                        if data_distribution[record[self.class_position]].get("max"):
                            data_distribution[record[self.class_position]]["max"] += 1
                        else:
                            data_distribution[record[self.class_position]]["max"] = 1
                    else:
                        if data_distribution[record[self.class_position]].get("min"):
                            data_distribution[record[self.class_position]]["min"] += 1
                        else:
                            data_distribution[record[self.class_position]]["min"] = 1

            distribution_attributes.append(data_distribution)
        return distribution_attributes

    def defineProbabilityByAttribute(self, distribution_attribute ):
        '''Calcular a tabela de Probabilidade Do atributo'''
        new_distribution_attribute = []
        for record in distribution_attribute[1:]:
            max = 0
            min = 0
            for key in (record.keys()):
                #if key not in "mediana attribute":
                if key not in "attribute":
                    try:
                        #max += float(record.get(key)["max"])
                        max += float(record.get(key)["max"])
                        min += float(record.get(key)["min"])
                        #min += float(record.get(key)["min"])
                    except:
                        pass

            record.update({"probability": {"max": (float(max)) / float(self.full_record)}})
            record["probability"]["min"] = (float(min)) / float(self.full_record)
            record.update({"full_max": max})
            record.update({"full_min": min})
            new_distribution_attribute.append(record)

        return new_distribution_attribute


    def calculateEntropyClass(self,probability_attribute):
        "Calculo da entropia classe"
        overall_entropy = 0
        for key in probability_attribute:
            overall_entropy += (self.entropy(float(key.get("probability")), 2))
        probability_attribute.append({"overall_entropy":overall_entropy})

        return overall_entropy, probability_attribute

    def calculateEntropyAttribute(self,probability_attribute):
            "Calculo da entropia de um atributo"

            for record in probability_attribute:
                entropy_max = 0
                entropy_min = 0

                for key in (record.keys()):
                    if key not in "mediana attribute probability full_max full_min":

                        if(float(record[key].get("max"))!=0.0 and float(record[key].get("min"))!=0.0):
                            entropy_max += (self.entropy(float(record[key].get("max"))/float(record["full_max"]), 2))
                            entropy_min += (self.entropy(float(record[key].get("min")) / float(record["full_min"]), 2))

                        elif float(record[key].get("max"))!=0.0:
                            entropy_max += (self.entropy(float(record[key].get("max")) / float(record["full_max"]), 2))

                        elif float(record[key].get("min"))!=0.0:
                            entropy_min += (self.entropy(float(record[key].get("min")) / float(record["full_min"]), 2))


                entropy_attribute = (float(record["probability"]["max"])*entropy_max)+(float(record["probability"]["min"])*entropy_min)
                gain_attribute = (self.entropy_class - entropy_attribute)
                record.update({"entropy_attribute": entropy_attribute})
                record.update({"gain_attribute": gain_attribute})


            return probability_attribute


    def mediana(self, attribute_position):
        "Calcula a mediana de uma lista de registros"
        point_average = []
        for record in self.data_list:
            point_average.append(record[attribute_position])

        #return median(point_average) #não funciona para registros ímpares
        return self.medianaCalc(point_average[1:])

    def medianaCalc(self,data):
        data = sorted(data) #ordena valores
        n = len(data)
        i = n/2
        if n%2 == 0:
            md = (float(data[i-1]) + float(data[i]))/2
        else:
            md = float(data[i])/2
        return md

    def entropy(self, pk, base):
        "calculo somente da entropia"
        if pk > 0:
            return -(pk * math.log(pk, base))



    def calculatingEntropyOfTheClass(self):
        "Calcula a antropia da classe"
        distribution_class = self.dataDistributionByClass(self.class_name)
        probability_class = self.defineProbabilityByClass(distribution_class)
        self.entropy_class, entropy_class = self.calculateEntropyClass(probability_class)


    def calculatingEntropyOfTheAttributes(self):
        "Calculo da Entropia e do Ganho"
        distribution_attribute = self.dataDistributionByAttribute()
        probability_attribute = self.defineProbabilityByAttribute(distribution_attribute)
        entropy_attributes = self.calculateEntropyAttribute(probability_attribute)
        attributes_max_gain =[]
        indice_gain = (0,0)



        for i, attributes in enumerate(entropy_attributes):
            if indice_gain[0] < attributes["gain_attribute"]:
                indice_gain = (attributes["gain_attribute"], i)
            attributes_max_gain.append([attributes["attribute"], attributes["gain_attribute"]])
            #attributes_max_gain.append([attributes["attribute"], attributes["mediana"], attributes["gain_attribute"]])

        return (attributes_max_gain[indice_gain[1]])





# Calculating_end_gain = EntropyAndGain()
# Calculating_end_gain.calculatingEntropyOfTheClass()
# print(Calculating_end_gain.calculatingEntropyOfTheAttributes()[0])
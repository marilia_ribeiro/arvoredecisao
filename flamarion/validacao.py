#!/usr/bin/env python
# -*- coding: utf-8 -*-
from arvore import Arvore

class validacaoArvore:
	acuracia = 0

	def validar(self, raiz, conjunto_de_testes):
		print("VALIDAÇÃO ÁRVORE")

		for registro_teste in conjunto_de_testes:
			valor = Arvore.classificarRegistro(raiz, registro_teste)
			
			if valor:
				self.acuracia+=1
		
		print("ACURÁCIA: %d" % self.acuracia)
		

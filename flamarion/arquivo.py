#!/usr/bin/env python
# -*- coding: utf-8 -*-

from statistics import median
import numpy as np

def arquivoTeste(arquivoTreinamento):
    linhaRegistro = (np.array(arquivoTreinamento.readlines()))
    matriz = []

    for linha in range(len(linhaRegistro)):
        matriz.append(linhaRegistro[linha].replace("\n", "").split(","))

    atributos = matriz[0]
    matriz = np.array(matriz).transpose()
    matriz = np.delete(matriz, 0, 1)
    matriz = matriz.astype(float)
    medianaColuna = []

    for linha in matriz:
        medianaColuna.append(median(linha))

    arquivoTeste = []
    matriz = np.array(matriz).transpose()

    for registro in matriz[1:]:
        dicionario = {}
        for i, atributo in enumerate(atributos):
            if(i == 0):
                dicionario[atributo] = registro[i]
            else:
                if registro[i] <= medianaColuna[i]:
                    dicionario[atributo] = "min"
                else:
                    dicionario[atributo] = "max"

        arquivoTeste.append(dicionario)
    return arquivoTeste

def main():
    arquivoTreinamento = open("train.txt", "r")
    dicionarioTeste = arquivoTeste(arquivoTreinamento)
    print(dicionarioTeste)
    return 0


if __name__ == '__main__':
    main()

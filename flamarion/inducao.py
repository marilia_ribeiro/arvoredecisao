#coding:utf-8
import copy
from arvore import Arvore
from validacao import *
from preparaDados import *

class_name = "class"

#football_training = open("football.txt", "r")
wine_training = open("wine_training.txt", "r")
#wine_training_2 = open("wine_training2.txt", "r")
wine_training_completo = open("wine_training_completo.txt", "r")

treinamento = arquivoTeste(wine_training_completo)

# IDENTIFICAÇÃO DE ATRIBUTOS
atributos = []
exemplos = treinamento
#listando os atributos
for exemplo in exemplos:
  	for chave in exemplo:
  		atributos.append(chave)
  	break
del atributos[atributos.index(class_name)]

# ============================
# INDUÇÃO DA ÁRVORE DE DECISÃO
# ============================
arv = Arvore()
arv.class_name = class_name # definir class_name

arv.induzirArvore(atributos, exemplos) # Induzir arvore

# =================================
# VISUALIZAÇÃO DA ÁRVORE DE DECISÃO
# =================================
arv.visualizar() # visualizar estrutura

# ==========================
# TESTE DA ÁRVORE DE DECISÃO
# ==========================
testes = arquivoTeste(wine_training)
raiz_da_arvore = copy.copy(arv.raiz)

# teste
arv.classificarItensDesconhecidos(raiz_da_arvore, testes)

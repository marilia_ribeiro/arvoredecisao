#coding: utf-8
from atributo import Atributo
from data import *
conjunto_treinamento = base['data']

atributos = []
for atributo in base['atributos']:
	a = Atributo(atributo, {})
	atributos.append(a)

for atributo in atributos:
	nome_atributo = atributo.nome
	for dado in conjunto_treinamento:
		atributo.adicionarValor(dado[nome_atributo])
	#atributo.mostrarValores()

at_motor = atributos[0]
at_rodas = atributos[1]

valores_motor = at_motor.valores

for registro in conjunto_treinamento:
	valores_motor[registro[at_motor.nome]]['registros'].append(registro)

# for chave, valor in valores_motor.iteritems():
# 	print('%s' %chave)
# 	for registro in valor['registros']:
# 		print(registro)

valores_rodas = at_rodas.valores

for chave, valor in valores_motor.iteritems():
	#print(chave)
	for registro in valor['registros']:
		#print(registro)
		valores_rodas[registro[at_rodas.nome]]['registros'].append(registro)

for chave, valor in valores_motor.iteritems():
	print('%s' %chave.upper())

	for registro in valor['registros']:
		print(registro)
	#for chave_roda, valor_roda in valor['registros']:
	#	print('%d' %chave_roda)
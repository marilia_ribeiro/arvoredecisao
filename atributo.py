#coding: utf-8

class Atributo():

    nome = ""
    valores = {}

    def __init__(self, nome, valores={}):
        self.nome = nome
        self.valores = valores

    def adicionarValor(self, valor):
        if valor not in self.valores:
            self.valores[valor] = {'frequencia': 1, 'registros': []}
        else:
            self.valores[valor]['frequencia'] += 1

    def mostrarValores(self):
        print('\nAtributo: %s' %self.nome)
        for chave, valor in self.valores.iteritems():
            print "> %s (freq: %s)" %(chave, valor['frequencia'])
#coding:utf-8
from data import *
data = base['data']

def identifica_classes(data):
	classes = {}
	for d in data:
		rotulo = d["rotulo"]
		if rotulo != None:
			if rotulo in classes:
				frequencia_atual = classes[rotulo]['frequencia']
				classes[rotulo] = {'frequencia': frequencia_atual+1}
			else:
				classes[rotulo] = {'frequencia': 1}
	return classes

def particiona(data, atributo):
	subconjuntos = {}
	for item in data:
		valor_atributo = item[atributo]

		if valor_atributo in subconjuntos:
			subconjuntos[valor_atributo]['f'] += 1
			subconjuntos[valor_atributo]['data'].append(item)
		else:
			subconjuntos[valor_atributo] = {
				'f': 1,
				'data': [item]
			}
	return subconjuntos

def ver_no(no):
	for valor, info in no.iteritems():
		print('<%s> - %d ocorrencias'  %(valor, info['f']))

classes_identificadas = identifica_classes(data)
#print(classes_identificadas)

def cria_arvore(data):
	a1 = "motor"
	a2 = "rodas"
	
	divisao_atributo_motor = particiona(data, a1) #motor
	
	#visualizar(a1, divisao_atributo_motor)
	#print(divisao_atributo_motor)
	for d in divisao_atributo_motor:
		particao = divisao_atributo_motor[d]

		print("'- MOTOR: %s" %d.upper())
		
		divisao_atributo_rodas = particiona(particao['data'], a2)

		for d2 in divisao_atributo_rodas:
			print("\t'--- RODAS: %s" %d2)
			particao_rodas = divisao_atributo_rodas[d2]
			for item in particao_rodas['data']:
				print("\t\t\t> %s" %item['rotulo'])
			#print(d2)
		#particao['data'])
	
	pass

def visualizar(atributo, particoes):
	for parte in particoes:
		print '\n%s: %s' %(atributo, parte)
		bla = particoes[parte]
		for b in bla['data']:
			print(b)

arvore = cria_arvore(base['data'])

# atributo_1 = "motor"
# atributo_2 = "rodas"

# no = entropia(data, atributo_1)

# #ver_no(no)
# for ramo, info in no.iteritems():
# 	novo_no = entropia(info['registros'], atributo_2)

# for item, valor in novo_no.iteritems():
# 	print(item)
# 	for i in valor['registros']:
# 		print(i)
# 	print('-------')

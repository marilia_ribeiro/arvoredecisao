#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy
from scipy.stats import entropy

def entropiaUmAtributo(probabilidades):
    '''
    chama a função entropy do python para ser aplicada para um único atributo
    entrada (probabilidade, opcional, base=2)
    Ex: class e outlook
    | class | fi |
    |  yes  |  9 |
    |  no   |  5 |
    |TOTAL  | 14 |
    
    E(T) = -p(i) * log p(i)
    onde:
    p = probabilidade
    i = frequencia do possível valor do atributo
    log = lê-se na base 2
    '''
    return entropy(probabilidades, None, 2) 

# esboço do que queremos
# não sei se vai ficar assim mesmo    
def entropiaDoisAtributos(noPai, noFilho):
    '''
    deve retornar o cálculo da entropia para a combinação entre dois atributos. 
    Ex: class e outlook
    | outlook \ class | yes | no | fi |
    |      sunny      |  3  | 2  | 5  |
    |     overcast    |  4  | 0  | 4  |
    |      rainy      |  2  | 3  | 5  |
    |TOTAL                       | 14 |
    
    E(T,X) = p(i) * E(i)
    '''
    #encontrar valores fequentes do noPai
    #encontrar valores frequentes do noFilho
    
    entropia = 0
    # ver certinho como vai ficar esse for
    #for valor in noFilho:
    #    entropia += probabilidade(frequenciaAcumuladaValor[i], fequenciaAcumuladaPai)*entropiaUmAtributo([probabilidade(frequenciaValor[i], frequenciaAcumuladaValor)])
    return entropia
    
def ganhoInformacao(entropiaNoPai, entropiaNoFilho):
    '''recebe:
       - valor da entropia do no pai (classe ou raiz atual)
       - valor da entropia do no filho
       retorna o cálculo do ganho da informação
       
       gi(T,X) = E(T) - E(T,X)
    '''
    return entropiaNoPai - entropiaNoFilho
    
def getGanhoInformacao(gis):
    '''recebe uma lista com os ganhos de informação dos atributos
       retorna o atributo que possui o maior ganho de informação
    '''
    return max(gis)
    #return atributo.index(max(gis)) #era a função index né?

def probabilidade(frequenciaAtributo, fequenciaAcumulada):
    return frequenciaAtributo/frequenciaAcumulada
    
